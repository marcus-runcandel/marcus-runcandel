<h1 align="center">
    <a href="https://git.io/typing-svg"><img src="https://readme-typing-svg.demolab.com?font=Anta&size=24&pause=1000&color=F78224&center=true&vCenter=true&random=false&width=435&lines=Hi+There!+%F0%9F%91%8B;I'm+Volodymyr+Artemenko!" alt="Typing SVG" /></a>
</h1>

<h3 align="center">A passionate full-stack web developer from Ukraine 🇺🇦</h3>

<br/>

<div align="center">

I'm currently working on **a marketplace**

I'm currrently learning **Docker, Supabase, AWS**

💬 Ask me about **Node.js, React, Firebase... or anything [here](https://github.com/salesp07/salesp07/issues)**

⚡ Fun fact **Fallout showed us that in the post-apocalyptic wasteland, the true currency is not gold or silver, but bottle caps. Nuka-Cola, anyone?**
</div>
<!--https://github.com/alexandresanlim/Badges4-README.md-Profile  BADGES-->
<div align="center"> 
  <a href="mailto:pedro.sales.muniz@gmail.com">
    <img src="https://img.shields.io/badge/ProtonMail-8B89CC?style=for-the-badge&logo=protonmail&logoColor=white" />
  </a>
  <a href="https://www.linkedin.com/in/vladimirart" target="_blank">
    <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank" />
  </a>
  <a href="https://salesp07.github.io" target="_blank">
     <img src="https://img.shields.io/badge/Portfolio-FF5722?style=for-the-badge&logo=todoist&logoColor=white" target="_blank" />
  </a>
</div>

<hr/>

<h2 align="center">⚒️ Languages-Frameworks-Tools ⚒️</h2>
<!--https://github.com/tandpfun/skill-icons icons url-->
<br/>
<div align="center">
    <img src="https://skillicons.dev/icons?i=react,angular,bootstrap,html,css,idea,materialui,gitlab,figma,tailwind,git" />
    <img src="https://skillicons.dev/icons?i=nodejs,python,androidstudio,javascript,typescript,express,php,firebase,mongodb,cpp,nextjs,mysql,nuxtjs" /><br>
</div>

<hr/>

<br/>

<div align="center">
<a href='' target='_blank'><img height='64' style='border:0px;height:64px;' src='https://storage.ko-fi.com/cdn/kofi1.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
</div>

<br/>